<?php
require_once "Model.php";

class Utilisateur {
    private string $login;
    private string $nom;
    private string $prenom;

    /**
     * @param string $login
     * @param string $nom
     * @param string $prenom
     */
    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    private static function builder(array $t) {
        return new static($t['login'], $t['nom'], $t['prenom']);
    }

    public static function getAllUtilisateur() {
        $pdo = Model::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM utilisateur");
        $res = [];
        foreach($pdoStatement as $user) {
            $res[] = self::builder($user);
        }
        return $res;
    }
}
?>