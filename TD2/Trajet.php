<?php
require_once "Model.php";

class Trajet {
    private int $id;
    private string $depart;
    private string $arrivee;
    private $date;
    private int $nbPlaces;
    private int $prix;
    private string $conducteur_login;

    /**
     * @param int $id
     * @param string $depart
     * @param string $arrivee
     * @param $date
     * @param int $nbPlaces
     * @param int $prix
     * @param string $conducteur_login
     */
    public function __construct(int $id, string $depart, string $arrivee, $date, int $nbPlaces, int $prix, string $conducteur_login)
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->nbPlaces = $nbPlaces;
        $this->prix = $prix;
        $this->conducteur_login = $conducteur_login;
    }

    private static function builder(array $t) {
        return new static($t['id'], $t['depart'], $t['arrive'], $t['date'], $t['nbPlaces'], $t['prix'], $t['conducteur_login']);
    }

    public static function getAllTrajet() {
        $pdo = Model::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM trajet");
        $res = [];
        foreach($pdoStatement as $trajet) {
            $res[] = self::builder($trajet);
        }
        return $res;
    }
}
?>
