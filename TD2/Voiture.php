<?php
require_once "Model.php";

class Voiture {
    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque(): string {
        return $this->marque;
    }

    // un setter
    public function setMarque(string $marque) {
        $this->marque = $marque;
    }

    // un constructeur
    public function __construct(
        string $marque,
        string $couleur,
        string $immatriculation,
        int $nbSieges
    ) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = substr($immatriculation, 0, 8);;
        $this->nbSieges = $nbSieges;
    }

    public static function construire(array $voitureFormatTableau) : ModelVoiture {
        return new static($voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges']);
    }

    public static function getVoitures() {
        $pdo = Model::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM voiture");
        $res = [];
        foreach($pdoStatement as $voiture) {
            $res[] = static::construire($voiture);
        }
        return $res;
    }

    /**
     * @return mixed
     */
    public function getCouleur(): string
    {
        return $this->couleur;
    }

    /**
     * @return mixed
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * @return mixed
     */
    public function getNbSieges()
    {
        return $this->nbSieges;
    }

    public function setImmatriculation(string $immatriculation) {
        $this->immatriculation = substr($immatriculation, 0, 8);
    }
    // une methode d'affichage.
    public function afficher() {
        echo("ModelVoiture $this->immatriculation de marque $this->marque (couleur $this->couleur, $this->nbSieges sieges)");
    }
}
?>