<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Liste voiture </title>
</head>

<body>
<?php
require_once './Voiture.php';

if (count($_POST) < 4) {
    var_dump($_POST);
    echo('Wrong params');
    return;
}

$voiture1 = new ModelVoiture($_POST['marque'], $_POST['couleur'], $_POST['immatriculation'], $_POST['nbSieges']);
$voiture1->sauvegarder();
?>
</body>
</html>