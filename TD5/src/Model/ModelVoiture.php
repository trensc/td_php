<?php
namespace App\Covoiturage\Model;

class ModelVoiture {
    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque(): string {
        return $this->marque;
    }

    // un setter
    public function setMarque(string $marque) {
        $this->marque = $marque;
    }

    // un constructeur
    public function __construct(
        string $marque,
        string $couleur,
        string $immatriculation,
        int $nbSieges
    ) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = substr($immatriculation, 0, 8);;
        $this->nbSieges = $nbSieges;
    }

    public static function construire(array $voitureFormatTableau) : ModelVoiture {
        return new static($voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges']);
    }

    public static function getVoitures() {
        $pdo = DatabaseConnection::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM voiture");
        $res = [];
        foreach($pdoStatement as $voiture) {
            $res[] = static::construire($voiture);
        }
        return $res;
    }

    /**
     * @return mixed
     */
    public function getCouleur(): string
    {
        return $this->couleur;
    }

    /**
     * @return mixed
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * @return mixed
     */
    public function getNbSieges()
    {
        return $this->nbSieges;
    }

    public function setImmatriculation(string $immatriculation) {
        $this->immatriculation = substr($immatriculation, 0, 8);
    }
    // une methode d'affichage.
    public function afficher() {
        echo("ModelVoiture $this->immatriculation de marque $this->marque (couleur $this->couleur, $this->nbSieges sieges)");
    }

    public static function deleteParImmat(string $immat): void {
        $sql = "DELETE FROM voiture WHERE immatriculation=:immatriculationTag";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = [
            "immatriculationTag" => $immat
        ];

        $pdoStatement->execute($values);
    }

    public static function getVoitureParImmat(string $immatriculation) : ?ModelVoiture {
        $sql = "SELECT * from voiture WHERE immatriculation=:immatriculationTag";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
        );
        $pdoStatement->execute($values);
        $voiture = $pdoStatement->fetch();
        return $voiture ? static::construire($voiture) : NULL;
    }

    public function sauvegarder() : void {
        $sql = "INSERT INTO voiture (immatriculation, marque, nbSieges, couleur) VALUES (:immatriculationTag, :marqueTag, :nbSiegesTag, :couleurTag)";
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $this->immatriculation,
            "marqueTag" => $this->marque,
            "nbSiegesTag" => $this->nbSieges,
            "couleurTag" => $this->couleur,
        );

        $pdoStatement->execute($values);
    }


}
?>