<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="./css/index.css" rel="stylesheet"/>
    <title><?php echo $pagetitle; ?></title>
</head>
<body>
<header>
    <nav>
        <a href="./frontController.php?action=readAll">Voitures</a>
        <a href="./frontController.php?action=readAll&controller=utilisateur">Users</a>
        <a href="./frontController.php?action=readAll&controller=trajet">Trajet</a>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de clément
    </p>
</footer>
</body>
</html>