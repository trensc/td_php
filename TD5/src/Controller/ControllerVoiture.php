<?php
namespace App\Covoiturage\Controller;

use App\Covoiturage\Model\ModelVoiture;

class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $vs = ModelVoiture::getVoitures(); //appel au modèle pour gerer la BD
        self::afficheVue("./view.php", [
            'voitures' => $vs,
            "pagetitle" => "Liste des voitures",
            "cheminVueBody" => "voiture/list.php"
        ]);
    }

    public static function read(): void {
        $immat = htmlspecialchars($_GET['immat']);
        $voiture = ModelVoiture::getVoitureParImmat($immat);
        if (!$voiture) {
            $errorCode = "Couldn't get voiture with immat: {$_GET['immat']}";
            self::afficheVue('voiture/error.php', [
                'errorCode' => $errorCode
            ]);
        } else {
            self::afficheVue('./view.php', [
                'voiture' => $voiture,
                "pagetitle" => "Detail voiture",
                "cheminVueBody" => 'voiture/details.php'
            ]);
        }
    }

    public static function create(): void {
        self::afficheVue("voiture/create.php");
    }

    public static function created(): void {
        $voiture = new ModelVoiture(
            htmlspecialchars($_GET['marque']),
            htmlspecialchars($_GET['couleur']),
            htmlspecialchars($_GET['immatriculation']),
            htmlspecialchars($_GET['nbSieges'])
        );
        $voiture->sauvegarder();
        self::afficheVue('./view.php', [
            'voiture' => $voiture,
            "pagetitle" => "Creation reussie voiture",
            "cheminVueBody" => 'voiture/created.php'
        ]);
    }

    public static function delete(): void {
        ModelVoiture::deleteParImmat(htmlspecialchars($_GET['immat']));
        self::readAll();
    }

    public static function error(string $errorMessage = "") {
        $msg = 'Problème avec la voiture ' . $errorMessage;
        self::afficheVue('./view.php', [
            'errorCode' => $msg,
            "pagetitle" => "Creation reussie voiture",
            "cheminVueBody" => 'voiture/error.php'
        ]);
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../view/$cheminVue"; // Charge la vue
    }
}
?>