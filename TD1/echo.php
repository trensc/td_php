<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Liste voiture </title>
</head>

<body>
<h1>Liste de voitures</h1>
<ul>
    <?php
    $voiture = [
        "marque" => "Konigsegg",
        "couleur" => "Grise",
        "immatriculation" => "1111",
        "nbSieges" => 2
    ];

    $voiture2 = [
        "marque" => "Ferrari",
        "couleur" => "Rouge",
        "immatriculation" => "YZTE7",
        "nbSieges" => 2
    ];

    $voiture3 = [
        "marque" => "Audi",
        "couleur" => "Noir",
        "immatriculation" => "3333",
        "nbSieges" => 5
    ];

    $voitures = [$voiture, $voiture2, $voiture3];

    for ($i = 0; $i < count($voitures); $i++) {
        $current = $voitures[$i];
        $str = "<li> ModelVoiture #immatriculation de marque #marque (couleur #couleur, #nbSieges sieges) </li>";
        echo("{$voiture['immatriculation']}");
        foreach ($current as $key => $value) {
            $str = str_replace("#$key", $value, $str);
        }

        echo($str);
    }

    ?>
</ul>
</body>
</html>