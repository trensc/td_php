<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Liste voiture </title>
</head>

<body>
<?php
require_once './ModelVoiture.php';

if (count($_POST) < 4) {
    echo('Wrong params');
    return;
}

$voiture1 = new ModelVoiture($_POST['marque'], $_POST['couleur'], $_POST['immatriculation'], $_POST['nbSieges']);
$voiture1->afficher();
?>
</body>
</html>