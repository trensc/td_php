<?php
    require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

    use App\Covoiturage\Lib\PreferenceControleur;
    use App\Covoiturage\Model\HTTP\Session;

    $loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
    $loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');
    $loader->register();

    $action = $_GET['action'] ?? "readAll";
    $controller = $_GET['controller'] ?? PreferenceControleur::lire();

    $controllerClassName = "App\Covoiturage\Controller\Controller" . ucfirst($controller);
    $session = Session::getInstance();
    if (!class_exists($controllerClassName))
        return require ControllerVoiture::error("No controller found for " . $controllerClassName);

    $controllerClassName::$action();
?>