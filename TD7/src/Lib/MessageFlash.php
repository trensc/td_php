<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Model\HTTP\Session;

class MessageFlash
{

    // Les messages sont enregistrés en session associée à la clé suivante
    private static string $cleFlash = "_messagesFlash";

    // $type parmi "success", "info", "warning" ou "danger"
    public static function ajouter(string $type, string $message): void
    {
        $tmp = self::lireTousMessages();
        $tmp[$type][] = $message;
        Session::getInstance()->enregistrer(self::$cleFlash, $tmp);
    }

    public static function contientMessage(string $type): bool
    {
        $s = Session::getInstance();
        return $s->contient(self::$cleFlash) && isset($s->lire(self::$cleFlash)[$type]);
    }

    // Attention : la lecture doit détruire le message
    public static function lireMessages(string $type): array
    {
        $arr = [];
        $s = Session::getInstance();
        if (self::contientMessage($type)) {
            $tmp = $s->lire(self::$cleFlash);
            $arr = $tmp[$type];
            unset($tmp[$type]);
            $s->enregistrer(self::$cleFlash, $tmp);
        }
        return $arr;
    }

    public static function lireTousMessages() : array
    {
        $arr = [];
        foreach(["success", "info", "warning", "error"] as $key) {
            $arr[$key] = self::lireMessages($key);
        }
        return $arr;
    }

}