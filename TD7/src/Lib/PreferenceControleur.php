<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Model\HTTP\Cookie;

class PreferenceControleur {
    private static string $clePreference = "preferenceControleur";

    public static function enregistrer(string $preference) : void
    {
        self::supprimer();
        Cookie::enregistrer(static::$clePreference, $preference);
    }

    public static function lire() : string
    {
        if (self::existe())
            return Cookie::lire(static::$clePreference);
        return "voiture";
    }

    public static function existe() : bool
    {
        return Cookie::contient(static::$clePreference);
    }

    public static function supprimer() : void
    {
        if (static::existe())
            Cookie::supprimer(static::$clePreference);
    }
}