<?php
namespace App\Covoiturage\Controller;

use App\Covoiturage\Model\DataObject\Voiture;
use App\Covoiturage\Model\Repository\VoitureRepository;
use App\Covoiturage\Lib\MessageFlash;

class ControllerVoiture extends GenericController {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $vs = (new VoitureRepository())->selectAll();
        self::afficheVue("./view.php", [
            'voitures' => $vs,
            "pagetitle" => "Liste des voitures",
            "cheminVueBody" => "voiture/list.php"
        ]);
    }

    public static function read(): void {
        $immat = htmlspecialchars($_GET['immat']);
        $voiture = (new VoitureRepository())->select($immat);
        if (!$voiture) {
            $errorCode = "Couldn't get voiture with immat: {$_GET['immat']}";
            self::afficheVue('voiture/error.php', [
                'errorCode' => $errorCode
            ]);
        } else {
            self::afficheVue('./view.php', [
                'voiture' => $voiture,
                "pagetitle" => "Detail voiture",
                "cheminVueBody" => 'voiture/details.php'
            ]);
        }
    }

    public static function create(): void {
        self::afficheVue("voiture/create.php");
    }

    public static function created(): void {
        $voiture = new Voiture(
            htmlspecialchars($_GET['marque']),
            htmlspecialchars($_GET['couleur']),
            htmlspecialchars($_GET['immatriculation']),
            htmlspecialchars($_GET['nbSieges'])
        );
        VoitureRepository::sauvegarder($voiture);
        MessageFlash::ajouter('success', 'La voiture ' . $voiture->getImmatriculation() . ' a bien été crée.');
        header("Location: ./frontController.php");
        exit();
    }

    public static function delete(): void {
        VoitureRepository::deleteParImmat(htmlspecialchars($_GET['immat']));
        $vs = (new VoitureRepository())->selectAll();
        MessageFlash::ajouter("success", "La voiture " . $_GET['immat'] . " a bien été supprimé");
        header("Location: ./frontController.php?action=readAll&controller=voiture");
        exit();
    }

    public static function update() {
        $voiture = VoitureRepository::getVoitureParImmat($_GET['immat']);
        self::afficheVue('voiture/update.php', [
            'voiture' => $voiture
        ]);
    }

    public static function updated() {
        $voiture = new Voiture(
            htmlspecialchars($_GET['marque']),
            htmlspecialchars($_GET['couleur']),
            htmlspecialchars($_GET['immatriculation']),
            htmlspecialchars($_GET['nbSieges'])
        );
        VoitureRepository::mettreAJour($voiture);

        MessageFlash::ajouter('success', 'La voiture ' . $voiture->getImmatriculation() . ' a bien été modifiée.');
        header("Location: ./frontController.php?action=readAll");
        exit();
    }

    public static function error(string $errorMessage = "") {
        $msg = 'Problème avec la voiture ' . $errorMessage;
        self::afficheVue('./view.php', [
            'errorCode' => $msg,
            "pagetitle" => "Creation reussie voiture",
            "cheminVueBody" => 'voiture/error.php'
        ]);
    }
}
?>