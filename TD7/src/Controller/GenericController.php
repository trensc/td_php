<?php

namespace App\Covoiturage\Controller;

use App\Covoiturage\Lib\PreferenceControleur;
use App\Covoiturage\Lib\MessageFlash;

class GenericController {
    protected static function afficheVue(string $cheminVue, array $parametres = []) : void {
        //$msgs = ["msgs" => MessageFlash::lireTousMessages()];
        $msgs = MessageFlash::lireTousMessages();
        $arr = array_merge($parametres, $msgs);
        extract($arr); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../view/$cheminVue"; // Charge la vue
    }

    public static function formulairePreference() {
        self::afficheVue("view.php", [
            "checked" => PreferenceControleur::lire(),
            "cheminVueBody" => "/utilisateur/formulairePreference.php"
        ]);
    }

    public static function enregistrerPreference() {
        if (isset($_GET["controleur_defaut"]))
            PreferenceControleur::enregistrer($_GET["controleur_defaut"]);
        $_SESSION['success'] = 'Préférences enregistrée!';
        header("Location: ./frontController.php");
        exit();
    }
}