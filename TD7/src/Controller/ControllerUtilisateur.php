<?php
namespace App\Covoiturage\Controller;

use App\Covoiturage\Model\Repository\UtilisateurRepository;
use App\Covoiturage\Model\Repository\VoitureRepository;
use App\Covoiturage\Model\HTTP\Cookie;
use App\Covoiturage\Model\HTTP\Session;

class ControllerUtilisateur extends GenericController {
    public static function readAll() : void {
        $users = (new UtilisateurRepository())->selectAll(); //appel au modèle pour gerer la BD
        self::afficheVue("./view.php", [
            'utilisateurs' => $users,
            "pagetitle" => "Liste des utilisateurs",
            "cheminVueBody" => "utilisateur/list.php"
        ]);
    }

    public static function read(): void {
        $login = htmlspecialchars($_GET['login']);
        $user = (new UtilisateurRepository())->select($login);
        if (!$user) {
            $errorCode = "Couldn't get user with login: {$_GET['login']}";
            self::afficheVue('voiture/error.php', [
                'errorCode' => $errorCode
            ]);
        } else {
            self::afficheVue('./view.php', [
                'user' => $user,
                "pagetitle" => "Detail user",
                "cheminVueBody" => 'utilisateur/details.php'
            ]);
        }
    }

    public static function error(string $errorMessage = "") {
        $msg = 'Problème avec les users ' . $errorMessage;
        self::afficheVue('./view.php', [
            'errorCode' => $msg,
            "pagetitle" => "Error",
            "cheminVueBody" => 'utilisateur/error.php'
        ]);
    }

    public static function delete(): void {
        (new UtilisateurRepository())->delete(htmlspecialchars($_GET['login']));
        $users = (new VoitureRepository())->selectAll();
        self::afficheVue('./view.php', [
            'users' => $users,
            'login' => $_GET['login'],
            "pagetitle" => "Suppression d'user reussie",
            "cheminVueBody" => 'utilisateur/deleted.php'
        ]);
    }
    /*
    public static function deposerCookie(): void {
        Cookie::enregistrer("_px3", "zehudygezhidjenz");
        self::afficheVue('./view.php', [
            "pagetitle" => "Cookies",
            "cheminVueBody" => 'utilisateur/list.php'
        ]);
    }

    public static function lireCookie(): void {
        echo Cookie::lire("_px3");
    }
    */

    public static function create(): void {
        self::afficheVue("utilisateur/create.php");
    }

    public static function created(): void {
        $v = new UtilisateurRepository();
        $user = $v->construire($_GET);
        $v::sauvegarder($user);
        $vs = $v->selectAll();
        self::afficheVue('./view.php', [
            'user' => $user,
            'users' => $vs,
            "pagetitle" => "Creation reussie user",
            "cheminVueBody" => 'utilisateur/created.php'
        ]);
    }
};
?>