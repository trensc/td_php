<?php
namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository {
    protected function getNomTable(): string{
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string{
        return "login";
    }

    protected function getNomsColonnes(): array
    {
        return [];
    }

    public function construire(array $ut): Utilisateur {
        return new Utilisateur($ut['login'], $ut['nom'], $ut['prenom']);
    }
};
?>