<?php
namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\DataObject\Voiture;

class VoitureRepository extends AbstractRepository {
    protected function getNomTable(): string{
        return "voiture";
    }

    protected function getNomClePrimaire(): string{
        return "immatriculation";
    }

    public function getNomsColonnes(): array
    {
        return [

        ];
    }

    public function construire(array $voitureFormatTableau) : Voiture {
        return new Voiture($voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges']);
    }

    public static function deleteParImmat(string $immat): void {
        $sql = "DELETE FROM voiture WHERE immatriculation=:immatriculationTag";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = [
            "immatriculationTag" => $immat
        ];

        $pdoStatement->execute($values);
    }

    public static function getVoitureParImmat(string $immatriculation) : ?Voiture {
        $sql = "SELECT * from voiture WHERE immatriculation=:immatriculationTag";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
        );
        $pdoStatement->execute($values);
        $voiture = $pdoStatement->fetch();
        return $voiture ? static::construire($voiture) : NULL;
    }

    public static function sauvegarder(Voiture $car) : bool {
        $sql = "INSERT INTO voiture (immatriculation, marque, nbSieges, couleur) VALUES (:immatriculationTag, :immatriculationTag, :nbSiegesTag, :couleurTag)";
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = array(
            "immatriculationTag" => $car->getImmatriculation(),
            "marqueTag" => $car->getMarque(),
            "nbSiegesTag" => $car->getNbSieges(),
            "couleurTag" => $car->getCouleur(),
        );

        return $pdoStatement->execute($values);
    }
}
?>
