<?php
namespace App\Covoiturage\Model\DataObject;

class Utilisateur extends AbstractDataObject {
    private $nom;
    private string $login;
    private string $prenom;

    public function __construct(
        string $login,
        string $nom,
        string $prenom,
    ) {
        $this->prenom = $prenom;
        $this->nom = $nom;
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function afficher() {
        echo("Utilisateur $this->login,  $this->prenom $this->nom");
    }

}
?>
