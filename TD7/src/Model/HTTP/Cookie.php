<?php
namespace App\Covoiturage\Model\HTTP;

class Cookie {
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        setcookie($cle, serialize($valeur), $dureeExpiration ? $dureeExpiration + time() : 0);
    }

    public static function lire(string $cle): mixed {
        return unserialize($_COOKIE[$cle]);
    }

    public static function contient($cle) : bool {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle): void {
        if (self::contient($cle)) {
            self::enregistrer($cle, "", 1);
            unset($_COOKIE[$cle]);
        }

    }
}