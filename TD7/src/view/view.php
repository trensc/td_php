<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="../assets/css/index.css" rel="stylesheet"/>
    <title><?php echo $pagetitle; ?></title>
</head>
<body>
<header>
    <nav>
        <a href="./frontController.php?action=readAll&controller=voiture">Voitures</a>
        <a href="./frontController.php?action=readAll&controller=utilisateur">Users</a>
        <a href="./frontController.php?action=readAll&controller=trajet">Trajet</a>
        <a href="./frontController.php?action=formulairePreference">
            <img src="../assets/img/heart.png"/>
        </a>
    </nav>
</header>
<main>
    <?php
    foreach ($msgs as $keys => $msg) {
        foreach($msg as $key => $value) {
            echo '<div class="alert alert-' . $keys. '">' . $value . '</div>';
        }
    }
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de clément
    </p>
</footer>
</body>
</html>