<?php
echo("
<body>
<form method='GET' action='./frontController.php'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <input type='hidden' name='action' value='updated' />
            <label for='immat_id'>Immatriculation</label> :
            <input type='text' readonly placeholder='256AB34' value={$voiture->getImmatriculation()} name='immatriculation' id='immat_id' required/>

            <label for='marque_id'>Marque</label> :
            <input type='text' placeholder='Ferrari' value={$voiture->getMarque()} name='marque' id='marque_id' required/>

            <label for='coul_id'>Couleur</label> :
            <input type='text' placeholder='256AB34' value={$voiture->getCouleur()} name='couleur' id='coul_id' required/>

            <label for='sieges_id'>Nb Sieges</label> :
            <input type='number' placeholder='2' value={$voiture->getNbSieges()} name='nbSieges' id='sieges_id' required/>
        </p>
        <p>
            <input type='submit' value='Envoyer' />
        </p>
    </fieldset>
</form>
</body>
");
?>