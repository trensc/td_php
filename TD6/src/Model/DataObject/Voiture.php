<?php
namespace App\Covoiturage\Model\DataObject;

class Voiture extends AbstractDataObject {
    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque(): string {
        return $this->marque;
    }

    // un setter
    public function setMarque(string $marque): void {
        $this->marque = $marque;
    }

    // un constructeur
    public function __construct(
        string $marque,
        string $couleur,
        string $immatriculation,
        int $nbSieges
    ) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = substr($immatriculation, 0, 8);;
        $this->nbSieges = $nbSieges;
    }

    /**
     * @return mixed
     */
    public function getCouleur(): string
    {
        return $this->couleur;
    }

    /**
     * @return mixed
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * @return mixed
     */
    public function getNbSieges()
    {
        return $this->nbSieges;
    }

    public function setImmatriculation(string $immatriculation) {
        $this->immatriculation = substr($immatriculation, 0, 8);
    }
    // une methode d'affichage.
    public function afficher() {
        echo("Voiture $this->immatriculation de marque $this->marque (couleur $this->couleur, $this->nbSieges sieges)");
    }
}
?>