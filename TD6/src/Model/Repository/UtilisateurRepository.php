<?php
namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository {
    protected function getNomTable(): string{
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string{
        return "login";
    }

    public function construire(array $ut): Utilisateur {
        return new Utilisateur($ut['login'], $ut['nom'], $ut['prenom']);
    }
};
?>