<?php
namespace App\Covoiturage\Controller;

use App\Covoiturage\Model\DataObject\Voiture;
use App\Covoiturage\Model\Repository\VoitureRepository;

class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $vs = (new VoitureRepository())->selectAll();
        self::afficheVue("./view.php", [
            'voitures' => $vs,
            "pagetitle" => "Liste des voitures",
            "cheminVueBody" => "voiture/list.php"
        ]);
    }

    public static function read(): void {
        $immat = htmlspecialchars($_GET['immat']);
        $voiture = (new VoitureRepository())->select($immat);
        if (!$voiture) {
            $errorCode = "Couldn't get voiture with immat: {$_GET['immat']}";
            self::afficheVue('voiture/error.php', [
                'errorCode' => $errorCode
            ]);
        } else {
            self::afficheVue('./view.php', [
                'voiture' => $voiture,
                "pagetitle" => "Detail voiture",
                "cheminVueBody" => 'voiture/details.php'
            ]);
        }
    }

    public static function create(): void {
        self::afficheVue("voiture/create.php");
    }

    public static function created(): void {
        $voiture = new Voiture(
            htmlspecialchars($_GET['marque']),
            htmlspecialchars($_GET['couleur']),
            htmlspecialchars($_GET['immatriculation']),
            htmlspecialchars($_GET['nbSieges'])
        );
        VoitureRepository::sauvegarder($voiture);
        $vs = (new VoitureRepository())->selectAll();
        self::afficheVue('./view.php', [
            'voiture' => $voiture,
            'voitures' => $vs,
            "pagetitle" => "Creation reussie voiture",
            "cheminVueBody" => 'voiture/created.php'
        ]);
    }

    public static function delete(): void {
        VoitureRepository::deleteParImmat(htmlspecialchars($_GET['immat']));
        $vs = (new VoitureRepository())->selectAll();
        self::afficheVue('./view.php', [
            'voitures' => $vs,
            'immatriculation' => $_GET['immat'],
            "pagetitle" => "Creation reussie voiture",
            "cheminVueBody" => 'voiture/deleted.php'
        ]);
    }

    public static function update() {
        $voiture = VoitureRepository::getVoitureParImmat($_GET['immat']);
        self::afficheVue('voiture/update.php', [
            'voiture' => $voiture
        ]);
    }

    public static function updated() {
        $voiture = new Voiture(
            htmlspecialchars($_GET['marque']),
            htmlspecialchars($_GET['couleur']),
            htmlspecialchars($_GET['immatriculation']),
            htmlspecialchars($_GET['nbSieges'])
        );
        VoitureRepository::mettreAJour($voiture);

        $voitures = (new VoitureRepository())->selectAll();

        self::afficheVue('./view.php', [
            "pagetitle" => "Mise a jour reussie",
            "cheminVueBody" => 'voiture/updated.php',
            "immatriculation" => $_GET['immatriculation'],
            "voitures" => $voitures
        ]);
    }

    public static function error(string $errorMessage = "") {
        $msg = 'Problème avec la voiture ' . $errorMessage;
        self::afficheVue('./view.php', [
            'errorCode' => $msg,
            "pagetitle" => "Creation reussie voiture",
            "cheminVueBody" => 'voiture/error.php'
        ]);
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../view/$cheminVue"; // Charge la vue
    }
}
?>