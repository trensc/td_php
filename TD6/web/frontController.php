<?php
    require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

    use App\Covoiturage\Controller\ControllerVoiture;

    $loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
    $loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');
    $loader->register();

    $action = isset($_GET['action']) ? $_GET['action'] : "error";
    $controller = isset($_GET['controller']) ? $_GET['controller'] : 'voiture';

    $controllerClassName = "App\Covoiturage\Controller\Controller" . ucfirst($controller);

    if (!class_exists($controllerClassName))
        return require ControllerVoiture::error("No controller found for " . $controllerClassName);

    $controllerClassName::$action();
?>