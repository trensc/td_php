<?php
require_once ('../model/ModelVoiture.php'); // chargement du modèle
class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $vs = ModelVoiture::getVoitures(); //appel au modèle pour gerer la BD
        self::afficheVue("voiture/list.php", [
            'voitures' => $vs
        ]);
    }

    public static function read(): void {
        $immat = $_GET['immat'];
        $voiture = ModelVoiture::getVoitureParImmat($immat);
        if (!$voiture) {
            $errorCode = "Couldn't get voiture with immat: {$_GET['immat']}";
            self::afficheVue('voiture/error.php', [
                'errorCode' => $errorCode
            ]);
        } else {
            self::afficheVue('voiture/details.php', [
                'voiture' => $voiture
            ]);
        }
    }

    public static function create(): void {
        self::afficheVue("voiture/create.php");
    }

    public static function created(): void {
        $voiture = new ModelVoiture($_GET['marque'], $_GET['couleur'], $_GET['immatriculation'], $_GET['nbSieges']);
        $voiture->sauvegarder();
        self::readAll();
    }

    public static function delete(): void {
        ModelVoiture::deleteParImmat($_GET['immat']);
        self::readAll();
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../view/$cheminVue"; // Charge la vue
    }
}
?>